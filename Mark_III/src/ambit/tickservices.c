/*
 * tickservices.c
 *
 * Created: 11/15/2015 9:26:08 PM
 *  Author: Ralph
 */ 

/****************************************************************** Includes */
#include <stdlib.h>
#include "tickservices.h"

/********************************************************** Data Definitions */
static TickNotify_t * TickNotify[TN_MAX_ENTRIES] = { NULL };

/****************************************************** Function Definitions */
TickNotify_t * addTickNotify(
	int count,
	int reload
)
{
	int i = 0;
	
	while( (i < TN_MAX_ENTRIES) && (TickNotify[i] != NULL) )
		i++;
		
	if( i < TN_MAX_ENTRIES ) {
		TickNotify[i] = (TickNotify_t *) malloc( sizeof(TickNotify_t) );
		if( TickNotify[i] == NULL )
			return NULL;
		vSemaphoreCreateBinary( TickNotify[i]->xSemaphore );
		if( TickNotify[i]->xSemaphore == NULL ) {
			free( TickNotify[i] );
			TickNotify[i] = NULL;
			return NULL;
		}

		TickNotify[i]->TickCountReload = reload;
		TickNotify[i]->TickCount       = count;
		
		return TickNotify[i];
	}
	else
		return NULL;
}

void vApplicationTickHook(void)
{
	signed portBASE_TYPE xHigherPriorityTaskWoken;
//	int woken = 0;
	int i = 0;
		
	while( i < TN_MAX_ENTRIES ) {
		if( TickNotify[i] != NULL ) {
			if( (TickNotify[i]->TickCount == 0) && (TickNotify[i]->TickCountReload != 0) ) {
				free( TickNotify[i] );
				TickNotify[i] = NULL;
			}
			else {
				if( TickNotify[i]->TickCount != 0 ) {
					TickNotify[i]->TickCount -= 1;
					if( TickNotify[i]->TickCount == 0 ) {
						if( TickNotify[i]->TickCountReload != 0 )
							TickNotify[i]->TickCount = TickNotify[i]->TickCountReload;
						xSemaphoreGiveFromISR( TickNotify[i]->xSemaphore, &xHigherPriorityTaskWoken );
						if( xHigherPriorityTaskWoken == pdTRUE )
							portYIELD_WITHIN_API();
//							woken = 1;
					}
				}
			}
		}
		i += 1;
	}
	
//	if( woken )
//		portYIELD_WITHIN_API();
}
