/*
 * buttons.h
 *
 * Created: 11/26/2015 9:16:16 AM
 *  Author: Ralph
 */ 


#ifndef BUTTON_H_
#define BUTTON_H_



void button_update( void );
void button_task( void * pvParameters );

#endif /* BUTTON_H_ */