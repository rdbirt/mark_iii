/*
 * tickservices.h
 *
 * Created: 11/15/2015 9:23:59 PM
 *  Author: Ralph
 */ 

#ifndef TICKERVICES_H_
#define TICKERVICES_H_

/****************************************************************** Includes */
#include "FreeRTOS.h"
#include "semphr.h"

/******************************************************************* Defines */
#define TN_MAX_ENTRIES  4

/********************************************************** Data Definitions */
typedef struct TickNotify {
	xSemaphoreHandle    xSemaphore;
	int                 TickCount;
	int                 TickCountReload;
} TickNotify_t;


/******************************************************* Function Prototypes */
TickNotify_t * addTickNotify( int reload, int count );
void vApplicationTickHook( void );


#endif /* TICKERVICES_H_ */