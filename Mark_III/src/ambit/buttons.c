/*
 * buttons.c
 *
 * Created: 11/26/2015 9:18:34 AM
 *  Author: Ralph
 */ 

#include "ioport.h"
#include "buttons.h"
#include "..\ambit\tickservices.h"

void button_update(
	void
)
{
	ioport_port_mask_t a;
	ioport_port_mask_t b;
	int buttons = 0;
	
	a = ioport_get_port_level( IOPORT_PIOA, (1 << 31) | (1 << 26) );	
	b = ioport_get_port_level( IOPORT_PIOA, (1 << 15) | (1 << 14) );
	
	if( (a & (1 << 26)) == 0 )
		buttons |= 1;
	if( (b & (1 << 15)) == 0 )
		buttons |= 2;
	if( (a & (1 << 31)) == 0 )
		buttons |= 4;
//	if( (b & (1 << 14)) == 0 )
//		buttons |= 8;
		
	if( buttons )
		buttons |= 0xFFFF0000;
}

void button_task(
	void * pvParameters
)
{
	static TickNotify_t * TickNotify;
	
	TickNotify = addTickNotify( 10, 10 );
	if( TickNotify == NULL )
		for( ;; );

	for( ;; ) {
		if( xSemaphoreTake(TickNotify->xSemaphore, portMAX_DELAY) == pdTRUE )
			button_update();
	}
}
