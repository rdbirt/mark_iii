
#ifndef LCD_H__
#define LCD_H__

#include <stdint.h>
#include <stdbool.h>
#include "semphr.h"

typedef struct {
	uint8_t						  * buffer;
	uint8_t							y_min;
	uint8_t							y_max;
} lcd_region_t;

// Function to initialize the Sharp LCD, including the GPIO lines and SPI
// peripheral on the Nordic ARM chip.
void LCD_init(void);

// Function to write a line of data from the frame buffer to the LCD.
// firstP is true if this is the first (possibly only) line to be written
// lastP is true if this is the last (possibly only) line to be written
bool write_LCD_line(uint8_t *frmbffr, bool firstP, bool lastP, bool insideGlcd);

// Function to clear the Sharp LCD display.
void LCD_clear(void);

// Function to write a block (contiguous series of lines) of data from the frame buffer to the LCD.
// Data block starts at *frmbffr, is written to LCD, starting at startLine, for nLines.
bool write_LCD_block(uint8_t startLine, uint8_t nLines, bool insideGlcd);

void setLcdChipSelect(void);
void clearLcdChipSelect(void);
void spi_master_transfer(void *p_buf, uint32_t size);
void LCD_test_task(void *pvParameters);
void LCD_update(void);
void LCD_task(void *pvParameters);
void LCD_set_pixel(unsigned int pixel, uint8_t * line, const unsigned int colour);
int LCD_set_region( lcd_region_t * lcd_region, const uint8_t y_min, const uint8_t y_max, uint8_t * buffer );

#endif	// LCD_H__
