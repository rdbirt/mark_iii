#ifndef ASHLEY_AMP_H
#define ASHLEY_AMP_H

static const unsigned char ashley_amp[] = {
    0x42, 0x4d, 0x3e, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x00, 
    0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x00, 
    0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 
    0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe9, 0x00, 0x00, 
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xa0, 0x00, 0x01, 0xb8, 0x00, 0x7f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xff, 0xdb, 0x60, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe9, 0x00, 0x00, 
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xe7, 0x00, 0x00, 0x07, 0x00, 0x0f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x1b, 0x00, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe9, 0x00, 0x00, 
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xfd, 0xff, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xdb, 0x00, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe9, 0x00, 0x00, 
    0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xf8, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x40, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc9, 0x00, 0x00, 
    0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xfe, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0xe3, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0xd2, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc9, 0x00, 0x00, 
    0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xc6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 
    0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbe, 0x12, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc9, 0x20, 0x00, 
    0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x92, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc9, 0x20, 0x00, 
    0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe3, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x27, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x92, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x49, 0x20, 0x00, 
    0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0x9f, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xa4, 0x92, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x49, 0x20, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x8f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x9f, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x92, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x49, 0x20, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xcd, 0xb6, 0xd2, 0x48, 0x04, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x49, 0x20, 0x00, 
    0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xcf, 0xf7, 0xff, 0xff, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x03, 0x7f, 0xff, 0xf3, 0x6d, 0xb6, 0xda, 0x48, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x49, 0x20, 0x00, 
    0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xfe, 0xdf, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x7f, 0xf6, 0xdb, 0x6d, 0xb6, 0xda, 0x48, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x69, 0x30, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xdb, 0xfd, 0xf7, 0xfb, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x48, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x69, 0x30, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x04, 0xdb, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xfe, 0xdb, 0x6d, 0xf6, 0xfb, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x6d, 0xa4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x04, 0x93, 0x6d, 0xb6, 0xdb, 0x7f, 0xff, 0xdb, 
    0x6d, 0xf6, 0xdb, 0x6d, 0xbe, 0xd3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x08, 0x37, 0xdf, 0xed, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x6d, 0xb4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x6d, 0xb6, 0xdb, 0x6f, 0xb6, 0xdb, 
    0x6d, 0xbe, 0xdb, 0x6d, 0xb6, 0xd3, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0x86, 0xff, 0xed, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xa4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x4d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xff, 0xdb, 0x6d, 0xb6, 0xdb, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x41, 0x24, 0xff, 0xfd, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xb4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x7f, 0xff, 0xff, 0x6d, 0xa6, 0xda, 0x6c, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x49, 0x30, 0xff, 0xfd, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xb4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xef, 0xb6, 0xda, 0x6c, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x48, 0x24, 0x9f, 0xfd, 0xb6, 0xda, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0xb4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x26, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x7f, 0xb6, 0xdb, 0xcd, 0x80, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x4c, 0x24, 0x1f, 0xfd, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb4, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xff, 0xf6, 0xdb, 0x4d, 0x80, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0d, 0x04, 0x9b, 0xed, 0xb6, 0xdb, 0x49, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xef, 0xbe, 0xda, 0x69, 0xa0, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x08, 0x04, 0x13, 0xed, 0xb6, 0xdb, 0x69, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x9b, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xff, 0xf6, 0xdb, 0xe9, 0x30, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0x00, 0x83, 0x6d, 0xb6, 0xdb, 0x6d, 0x24, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x93, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdf, 0x6d, 0xb6, 0xdb, 0x4d, 0x20, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0x00, 0x93, 0xed, 0xb6, 0xdb, 0x6d, 0xb4, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xed, 0xb6, 0xdb, 0x6c, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0x00, 0x93, 0x7d, 0xb6, 0xdb, 0x6d, 0xb4, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0xed, 0xb6, 0xdb, 0x6c, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x7d, 0xb6, 0xdb, 0x6d, 0xb4, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xb7, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x7d, 0x04, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xf6, 0xdb, 0x6d, 0xb6, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x4d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xa4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xf6, 0xdb, 0x6d, 0xb6, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x49, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xa0, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xf6, 0xdb, 0x6d, 0xb6, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49, 0x36, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xa4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xf6, 0xdb, 0x6d, 0xb6, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49, 0x24, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xf6, 0xdb, 0x6d, 0xb6, 0x92, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x24, 0x93, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xd2, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0x92, 
    0x49, 0x24, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xd2, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0x92, 
    0x49, 0x24, 0xdb, 0x4d, 0xb6, 0xdb, 0x6d, 0xb4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xda, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0x92, 
    0x49, 0x24, 0x92, 0x4d, 0x36, 0x9b, 0x6d, 0xb6, 0xd0, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xda, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x80, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x93, 0x6d, 0xb6, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x49, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x69, 0x34, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xfe, 0xff, 0x6d, 0xb6, 0xdb, 
    0x69, 0x24, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x09, 0x24, 0x92, 0x49, 0x24, 0x92, 0x4d, 0x24, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xff, 0xff, 0xed, 0xb6, 0xdb, 
    0x6d, 0xb4, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xff, 0xff, 0xff, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xff, 0xff, 0xff, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x49, 0x05, 0xb0, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x7d, 0xf6, 0x49, 0x24, 0x92, 0x48, 0x2c, 0x92, 0x49, 0x05, 0x96, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x49, 0x2c, 0x92, 0x49, 0x2c, 0x92, 0x49, 0x2d, 0x96, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x49, 0x6d, 0xb6, 0x49, 0x2c, 0x96, 0xdb, 0x6d, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x49, 0x6d, 0xb6, 0x49, 0x24, 0xb6, 0xdb, 0x6c, 0xb0, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xcb, 0x6d, 0xb6, 0xd9, 0x25, 0xb6, 0x5b, 0x25, 0xb2, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xdb, 0x6c, 0xb6, 0xdb, 0x24, 0xb2, 0x49, 0x6d, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0x24, 0x92, 0x48, 0x24, 0x92, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf6, 0xdb, 0x25, 0xb6, 0xdb, 0x25, 0xb6, 0x4b, 0x6d, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x49, 0x24, 0x92, 0x49, 0x00, 0x92, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xed, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x2d, 0xb6, 0xdb, 0x6d, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x4d, 0xb6, 0x92, 0x49, 0x20, 0x02, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xc0, 
    0x00, 0x00, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x80, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 
    0x6d, 0xb4, 0x92, 0x49, 0x24, 0x80, 0x49, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x49, 0xb6, 0xdb, 0x65, 0xb6, 0x40, 
    0x04, 0x80, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xc0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x80, 0x09, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0x34, 0x19, 0x24, 0x92, 0x49, 
    0x04, 0x02, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x80, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x92, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x90, 0x01, 0x24, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0xdb, 0x64, 0x00, 0xdb, 0x2c, 0x92, 0xdb, 
    0x20, 0x02, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0x49, 0x6d, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x9a, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x00, 0x04, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x06, 0xdb, 0x6c, 0x96, 0xcb, 0x20, 0x92, 0x4b, 
    0x64, 0x90, 0xdb, 0x6d, 0xb6, 0xd9, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x36, 0xd2, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x40, 0x00, 0x92, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x36, 0xdb, 0x6d, 0x96, 0xd9, 0x24, 0x92, 0x49, 
    0x24, 0x92, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0xa6, 0x92, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x48, 0x00, 0x12, 0x40, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0x49, 0x24, 0x92, 0x49, 
    0x24, 0x92, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x6d, 0x26, 0x92, 
    0x49, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x00, 0x82, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0x49, 0x20, 0x92, 0x49, 
    0x24, 0x92, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x69, 0x20, 0x10, 
    0x01, 0x24, 0x92, 0x49, 0x24, 0x92, 0x49, 0x20, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0x48, 0x00, 0x92, 0x48, 
    0x24, 0x92, 0x00, 0x00, 0x90, 0x01, 0x24, 0x00, 0x00, 0x00, 0x10, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93, 0x6d, 0x26, 0xd2, 
    0x49, 0x24, 0x02, 0x49, 0x24, 0x92, 0x49, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xd9, 0x24, 0x92, 0x48, 
    0x00, 0x12, 0x49, 0x24, 0x92, 0x41, 0x24, 0x00, 0x48, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9a, 0x6d, 0xb6, 0xdb, 
    0x69, 0x24, 0x80, 0x09, 0x24, 0x92, 0x49, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6c, 0x92, 0x49, 
    0x00, 0x92, 0xfb, 0x64, 0x92, 0x00, 0x24, 0x02, 0x48, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x93, 0x6d, 0xb6, 0xdb, 
    0x4d, 0xa4, 0x92, 0x01, 0x24, 0x92, 0x49, 0x24, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6c, 0x92, 0x49, 
    0x24, 0x92, 0xff, 0xff, 0xfe, 0xc9, 0x24, 0x92, 0x49, 0x24, 0x80, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x9b, 0x6d, 0xb4, 0x92, 
    0x49, 0xb6, 0xda, 0x40, 0x24, 0x9b, 0x49, 0x24, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0x92, 0x4b, 
    0x6d, 0xb6, 0xff, 0xff, 0xff, 0xff, 0xe5, 0x92, 0x49, 0x24, 0xb6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0xdb, 0x6d, 0xb4, 0x92, 
    0x49, 0x24, 0xda, 0x49, 0x24, 0x9b, 0x4d, 0x24, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0xff, 0xb6, 0xff, 
    0xfd, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x64, 0x90, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xb6, 0xdb, 0x69, 0x24, 0x92, 
    0x49, 0x24, 0x9b, 0x69, 0x34, 0xd3, 0x6d, 0x34, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x82, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0xb6, 0xdb, 0x69, 0xa4, 0x92, 
    0x49, 0x24, 0x93, 0x6d, 0xa4, 0x93, 0x6d, 0x34, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf6, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4d, 0xb6, 0xdb, 0x69, 0xb6, 0xdb, 
    0x49, 0x24, 0x9a, 0x6d, 0x24, 0x9b, 0x6d, 0x34, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x4d, 0xa4, 0x93, 0x6d, 0xa4, 0x9b, 0x69, 0x34, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0xb6, 0xdb, 0x6d, 0xbf, 0xff, 
    0xed, 0xb6, 0x92, 0x4d, 0xb6, 0xdb, 0x6d, 0xb4, 0x90, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x6d, 0xb6, 0xdb, 0x6d, 0xbf, 0xff, 
    0xfd, 0xf6, 0x93, 0x6d, 0xb6, 0xdb, 0x6d, 0xb4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe3, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x12, 0x6d, 0xb6, 0xdb, 0x6d, 0xb7, 0xff, 
    0xfd, 0xb6, 0x9b, 0x6d, 0xb6, 0xdb, 0x6d, 0xa4, 0x80, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xff, 
    0xfd, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0xa4, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xff, 
    0xed, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x6d, 0xa6, 0xdb, 0x6d, 0xb6, 0xff, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x6d, 0x24, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x49, 0x24, 0x93, 0x6d, 0xb6, 0xff, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x69, 0x20, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe3, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x02, 0x49, 0x24, 0x92, 0x49, 0x36, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x92, 0x49, 0x36, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x80, 0x12, 0x49, 0x36, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x49, 0x36, 0xdb, 
    0x49, 0xb6, 0xdb, 0x6d, 0xb6, 0xdb, 0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x49, 0x36, 0xdb, 
    0x49, 0x24, 0x92, 0x4d, 0xb6, 0xdb, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
    0x00, 0x00, 0x00, 0x00, 0x04, 0x90, 0x00, 0x00, 0x00, 0x49, 0x36, 0xd2, 
    0x48, 0x24, 0x92, 0x49, 0xb6, 0xdb, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
    0x00, 0x00, 0x00, 0x00, 0x04, 0x92, 0x00, 0x00, 0x02, 0x49, 0x34, 0x92, 
    0x48, 0x00, 0x92, 0x49, 0x24, 0x9b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 0x08, 0x00, 0x12, 0x49, 0x24, 0x92, 
    0x40, 0x07, 0xf8, 0x09, 0x24, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x4d, 0x24, 0x92, 
    0x00, 0x06, 0xe0, 0x00, 0x04, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93, 0x6d, 0xb4, 0x92, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x02, 0x40, 0x24, 0xdb, 0x6d, 0xb6, 0x90, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x24, 0xdb, 0x6d, 0xb6, 0xd2, 
    0x01, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x02, 0x48, 0x04, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x40, 0x04, 0x9a, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x04, 0xdb, 0x6d, 0xb6, 0xdb, 
    0x68, 0x00, 0xdb, 0x6d, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49, 0x00, 0x9b, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xa0, 0x13, 0x6d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x92, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0x92, 0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x20, 0x13, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x24, 0x12, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xdb, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0x02, 0x4d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0xd2, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x24, 0x02, 0x6d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0x92, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x80, 0x4d, 0xb6, 0xdb, 
    0x6d, 0xb6, 0x92, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x90, 0x49, 0xb6, 0xdb, 
    0x6d, 0xb4, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x90, 0x0d, 0x26, 0xdb, 
    0x6d, 0xa4, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x92, 0x01, 0x24, 0xdb, 
    0x6d, 0xa4, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 0x01, 0x26, 0xdb, 
    0x6d, 0xa4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 0x01, 0x26, 0xdb, 
    0x6d, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x40, 0x24, 0xdb, 
    0x6d, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x24, 0x93, 
    0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x04, 0x92, 
    0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x92, 
    0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 
    0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 
    0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xe3, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xf1, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0x8e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xe4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff
};

#endif
