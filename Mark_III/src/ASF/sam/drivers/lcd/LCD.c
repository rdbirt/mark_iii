/*
 * Copyright (c) 2015 Ambit Network Systems. All Rights Reserved.
 */

#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "sleepmgr.h"
#include "flexcom.h"
#include "spi.h"
#include "ioport.h"
#include "delay.h"
#include "sharp_LS013B7DH06.h"
#include "LCD.h"
#include "..\ambit\tickservices.h"

/* Chip select. */
#define SPI_CHIP_SEL 0
#define SPI_CHIP_PCS spi_get_pcs(SPI_CHIP_SEL)

/* SPI clock setting (Hz). */
//static uint32_t gs_ul_spi_clock = 2500000;
static uint32_t gs_ul_spi_clock = 3000000;

/* Clock polarity. */
#define SPI_CLK_POLARITY 0

/* Clock phase. */
#define SPI_CLK_PHASE 0

/* Delay before SPCK. */
//#define SPI_DLYBS 0x40
#define SPI_DLYBS 0x10

/* Delay between consecutive transfers. */
//#define SPI_DLYBCT 0x10
#define SPI_DLYBCT 0x04

/** SPI base address for SPI master mode*/

// Working variables -- track state of frame buffer transfer to LCD.
uint8_t vcom = 0;		// Current state of vcom (pulsed at 60Hz)

#ifdef MANUAL_LCD_CS
void setLcdChipSelect(void){
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_HIGH);
}

void clearLcdChipSelect(
	void
)
{
	int i;
	
	/* Delay a bit or CS will go inactive before the transfer has finished */
	for( i = 0; i < 30; i++ )
		;
		
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_LOW);
}
#endif

/**
 * \brief Initialize SPI as master.
 */
static void spi_master_initialize(void)
{
#if (SAMG55)
	/* Enable the peripheral and set SPI mode. */
	flexcom_enable(BOARD_FLEXCOM_SPI);
	flexcom_set_opmode(BOARD_FLEXCOM_SPI, FLEXCOM_SPI);
#else
	/* Configure an SPI peripheral. */
	spi_enable_clock(SPI_MASTER_BASE);
#endif
	spi_disable(SPI_MASTER_BASE);
	spi_reset(SPI_MASTER_BASE);
	spi_set_lastxfer(SPI_MASTER_BASE);
	spi_set_master_mode(SPI_MASTER_BASE);
	spi_disable_mode_fault_detect(SPI_MASTER_BASE);
	spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_CHIP_PCS);
	spi_set_clock_polarity(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_POLARITY);
	spi_set_clock_phase(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_PHASE);
	spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_CHIP_SEL,
			SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(SPI_MASTER_BASE, SPI_CHIP_SEL,
			(sysclk_get_cpu_hz() / gs_ul_spi_clock));
	spi_set_transfer_delay(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_DLYBS,
			SPI_DLYBCT);
	spi_enable(SPI_MASTER_BASE);
}

/**
 * \brief Perform SPI master transfer.
 *
 * \param pbuf Pointer to buffer to transfer.
 * \param size Size of the buffer.
 */
void spi_master_transfer(void *p_buf, uint32_t size)
{
	uint32_t i;
//	uint8_t uc_pcs;
//	static uint16_t data;
	uint8_t *p_buffer = (uint8_t *) p_buf;

	for (i = 0; i < size; i++) {
		spi_write(SPI_MASTER_BASE, p_buffer[i], 0, 0);
		while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_TDRE) == 0)
			;
	//	while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);
	
	//	spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
	//	p_buffer[i] = data;
	}
}

void LCD_clear(void)
{
#ifdef LCD_SHARP_LS013B7DH06
	sharp_lcd_clear_screen();
#else
#error "NO LCD DEFINED!"
#endif
}

// Function to initialize the Sharp LCD, including the GPIO lines and SPI
// peripheral on the Nordic ARM chip.
// NOTE: Sequence of operations structured to follow Fig. 4, "Power Supply Sequencing" in document
// "Sharp LS013B7DH03 Application Information", p.7
void LCD_init(void)
{
	// Initialize SPI0 (SCLK, SI lines) and SCS GPIO line.
	spi_master_initialize();

	// T0 and T1 per Fig. 4.
	// Set up GPIO control lines (EXTCOMIN, DISP, EXTMODE)
	ioport_set_pin_dir(EXT1_PIN_LCD_ENABLE,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_dir(EXT1_PIN_LCD_EXTMODE,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_dir(EXT1_PIN_SPI_LCD_EXTCOM,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
#ifdef MANUAL_LCD_CS
	ioport_set_pin_dir(EXT3_PIN_GPIO_0,IOPORT_DIR_OUTPUT);		// Used to turn display on/off
	ioport_set_pin_level(EXT3_PIN_GPIO_0, IOPORT_PIN_LEVEL_HIGH);
#endif //MANUAL_LCD_CS

	ioport_set_pin_level(EXT1_PIN_LCD_ENABLE, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(EXT1_PIN_SPI_LCD_EXTCOM, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(EXT1_PIN_LCD_EXTMODE, IOPORT_PIN_LEVEL_HIGH);

	//delay
	delay_ms(1);										// Wait for LCD latches to settle

	// Fig. 4, T2
	LCD_clear();										// With LCD_DISP off, clear the display
	delay_ms(1);										// For safety, wait a bit.

	// Fig. 4, T3
	ioport_set_pin_level(EXT1_PIN_LCD_ENABLE, IOPORT_PIN_LEVEL_HIGH);	// Raise LCD_DISP to turn on display of pixels, will stay on...
	delay_ms(1);										// For safety, wait a bit.

	return;												// LCD now in running mode
}

int LCD_set_region(
	lcd_region_t	* lcd_region,
	const uint8_t	  y_min,
	const uint8_t	  y_max,
	uint8_t			* buffer
)
{
	if( (y_min > LCD_Y_RESOLUTION) || (y_min > y_max) )
		return -1;

	if( y_max > LCD_Y_RESOLUTION )
		return -1;

	if( buffer == NULL )
		return -1;

	lcd_region->y_min  = y_min;
	lcd_region->y_max  = y_max;
	lcd_region->buffer = buffer;

	return 0;
}

void LCD_set_pixel(
	const unsigned int	  pixel,
	uint8_t				* line,
	const unsigned int	  colour
)
{
	unsigned int d;
	unsigned int m;

	d = ((pixel * PIXEL_WIDTH) + PIXEL_WIDTH) / STORAGE_WIDTH;
	m = ((pixel * PIXEL_WIDTH) + PIXEL_WIDTH) % STORAGE_WIDTH;
	if( m < PIXEL_WIDTH ) {
		line[d-1] |= colour >> m;
		line[d+0] |= colour << (STORAGE_WIDTH - m);
	}
	else {
		line[d] |= colour << (STORAGE_WIDTH - m);
	}
}

#include "color.bmp.h"
#include "usertile13.amp.h"
#include "usertile41.amp.h"
#include "madman.amp.h"
#include "let_it_be.amp.h"
#include "tommy.amp.h"
#include "ashley.amp.h"
#include "breakfast.amp.h"
#include "yellow_brinck_road.amp.h"
#include "christy.amp.h"
#include "brits.amp.h"
#include "burn.amp.h"
#include "test3.amp.h"

static lcd_region_t lcd_region;
static int image = 0;

#define UPDATE_INTERVAL_MAX       (2)

void LCD_update(
)
{
	static int update = UPDATE_INTERVAL_MAX;
	
	vcom ^= LCD_VCOM_CONTROL;
	if( update++ >= UPDATE_INTERVAL_MAX )
		update = 0;

	if( update == 0 ) {
		switch( image ) {
			case 0:
				memcpy( lcd_region.buffer, color_bmp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 1:
				memcpy( lcd_region.buffer, usertile13_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 2:
				memcpy( lcd_region.buffer, usertile41_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 3:
				memcpy( lcd_region.buffer, ashley_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 4:
				memcpy( lcd_region.buffer, madman_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 5:
				memcpy( lcd_region.buffer, let_it_be_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 6:
				memcpy( lcd_region.buffer, tommy_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 7:
				memcpy( lcd_region.buffer, christy_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 8:
				memcpy( lcd_region.buffer, breakfast_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 9:
				memcpy( lcd_region.buffer, yellow_brinck_road_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 10:
				memcpy( lcd_region.buffer, burn_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 11:
				memcpy( lcd_region.buffer, brits_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			case 12:
				memcpy( lcd_region.buffer, test3_amp + 0x3E, LCD_BUFFER_SIZE );
				break;
			default:
				for( ;; );
		}
		lcd_write_bounded( &lcd_region, 255U, 255U );
		if( ++image >= 13 )
			image = 0;
	}
	else
		lcd_write_vcom();
}

void LCD_task(
	void	* pvParameters
)
{
	static TickNotify_t * LCDTickNotify;
	
	LCDTickNotify = addTickNotify( 1000U / portTICK_RATE_MS, 1000U / portTICK_RATE_MS );
	if( LCDTickNotify == NULL )
		for( ;; );

	lcd_region.y_min = 1U;
	lcd_region.y_max = LCD_Y_RESOLUTION;
	lcd_region.buffer = (uint8_t *) malloc( LCD_BUFFER_SIZE );

	for( ;; ) {
		if( xSemaphoreTake(LCDTickNotify->xSemaphore, portMAX_DELAY) == pdTRUE )
			LCD_update();
	}
}
