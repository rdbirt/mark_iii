
#include "FreeRTOS.h"
#include "string.h"
#include "LCD.h"
#include "sharp_LS013B7DH06.h"

uint8_t vcom;

uint8_t swap(
	uint8_t data
)
{
	uint8_t mask = 0x80;
	uint8_t out  = 0;
	uint8_t i;
	
	for( i = 1; i != 0; i <<= 1 ) {
		if( data & i )
			out |= mask;
		mask >>= 1;
	}
	
	return out;
}


void sharp_lcd_clear_screen(
	void
)
{
	uint8_t cmd_buff[2];

	cmd_buff[0] = LCD_CLEAR_MEMORY;
	cmd_buff[1] = 0;

	setLcdChipSelect();
	spi_master_transfer( (void *) cmd_buff, sizeof(cmd_buff) );
	clearLcdChipSelect();
}

void lcd_write_vcom(
)
{
	uint8_t cmd_buff[2];

	setLcdChipSelect();

	cmd_buff[0] = vcom;
	cmd_buff[1] = 0;
	spi_master_transfer( cmd_buff, 2U );

	clearLcdChipSelect();
}

void lcd_write_bounded(
	lcd_region_t	* lcd_region,
	const uint8_t	  y_start,
	const uint8_t	  y_stop
)
{
	static uint8_t cmd_buff[LCD_BYTES_PER_LINE + 3];
	uint8_t y_row_end = LCD_Y_RESOLUTION;
	uint8_t y_row = 1;

	if( y_start <= LCD_Y_RESOLUTION )
		y_row = y_start;
	else
		y_row = lcd_region->y_min;

	if( y_stop <= LCD_Y_RESOLUTION )
		y_row_end = y_stop;
	else
		y_row_end = lcd_region->y_max;

	configASSERT( y_row_end >= y_row );

	cmd_buff[sizeof(cmd_buff) - 1] = 0;
	cmd_buff[sizeof(cmd_buff) - 2] = 0;

	setLcdChipSelect();

	cmd_buff[0] = LCD_WRITE_LINE | vcom;
	cmd_buff[1] = 0;
	spi_master_transfer( cmd_buff, 1U );

	for( ; y_row < y_row_end; y_row++ ) {
		cmd_buff[0] = swap( y_row );
		memcpy( &cmd_buff[1], &(lcd_region->buffer[(y_row_end - 1 - (y_row - 1)) * LCD_BYTES_PER_LINE]), LCD_BYTES_PER_LINE );
		spi_master_transfer( cmd_buff, LCD_BYTES_PER_LINE + 2U );
	}

	cmd_buff[0] = swap( y_row_end );
	memcpy( &cmd_buff[1], lcd_region->buffer, LCD_BYTES_PER_LINE );
	spi_master_transfer( cmd_buff, LCD_BYTES_PER_LINE + 3U );

	clearLcdChipSelect();
}
