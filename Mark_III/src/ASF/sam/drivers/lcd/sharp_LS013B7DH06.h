#ifndef SHARP_LS013B7DH06_H_
#define SHARP_LS013B7DH06_H_

#define LCD_SHARP_LS013B7DH06

#include "LCD.h"

#define LCD_WRITE_LINE				0x80
#define LCD_VCOM_CONTROL			0x40
#define LCD_CLEAR_MEMORY			0x20

#define LCD_X_RESOLUTION			(128U)
#define LCD_Y_RESOLUTION			(128U)
#define LCD_BYTES_PER_LINE			(LCD_X_RESOLUTION * 3 / 8)
#define LCD_BUFFER_SIZE				(LCD_Y_RESOLUTION * LCD_BYTES_PER_LINE)

#define PIXEL_WIDTH					(3U)
#define STORAGE_WIDTH				(8U)

#define LCD_BLACK					0x00
#define LCD_BLUE					0x01
#define LCD_GREEN					0x02
#define LCD_CYAN					0x03
#define LCD_RED						0x04
#define LCD_MAGENTA					0x05
#define LCD_YELLOW					0x06
#define LCD_WHITE					0x07

void sharp_lcd_clear_screen( void );
void lcd_write_vcom( void );
void lcd_write_bounded( lcd_region_t * lcd_region, const uint8_t y_start, const uint8_t y_stop );
uint8_t swap( uint8_t data );

#endif /* SHARP_LS013B7DH06_H_ */
