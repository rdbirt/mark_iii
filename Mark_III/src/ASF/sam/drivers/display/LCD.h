/* Copyright (c) 2014 Ambit Network Systems. All Rights Reserved.
 *
 * The information contained herein is property of Ambit Systems.
 *
 *  This heading must NOT be removed from the file.
 *	By Mike Wirth, March, 2014.  Loosely based on the LCD
 *	App Note from Sharp, by Ken Green.
 *
 */


#ifndef LCD_H__
#define LCD_H__

#include <stdint.h>
#include <stdbool.h>


/**
 * Driver for a Sharp LCD, LS013B7DH03.
 */

// Commands (NOTE: DELIVERED TO LCD LSB FIRST PER SPI SETUP!)
#define MLCD_WR	0x01		// MLCD write line, for LSB first
#define MLCD_CM	0x04		// MLCD clear memory, for LSB first
#define MLCD_NO	0x00		// MLCD NOP (used to switch VCOM)

// LCD resolution
#define MLCD_XRES	128		// Pixels per horizontal line (limited to multiple of 8 internal to this code).
#define MLCD_YRES	128		// Pixels per vertical line (limited to 255 line internal to this code)
#define MLCD_BYTES_PER_LINE	(MLCD_XRES / 8)	// Number of bytes per line
#define MLCD_BUF_SIZE	(MLCD_YRES * MLCD_BYTES_PER_LINE)	// Size of frame buffer

// VCOM bit in command byte (NOTE: DELIVERED TO LCD LSB FIRST!)
#define VCOM_HI	0x02			// for LSB first
#define VCOM_LO	0x00

// External variables
extern uint8_t	vcom;	// Current state of vcom (toggled at 60Hz)
extern uint8_t  LCDframeBuffer[];
typedef uint8_t	LcdFBYXType[][MLCD_BYTES_PER_LINE];

// Function to initialize the Sharp LCD, including the GPIO lines and SPI
// peripheral on the Nordic ARM chip.
void LCD_init(void);

// Function to write a line of data from the frame buffer to the LCD.
// firstP is true if this is the first (possibly only) line to be written
// lastP is true if this is the last (possibly only) line to be written
bool write_LCD_line(uint8_t *frmbffr, bool firstP, bool lastP, bool insideGlcd);

// Function to clear the Sharp LCD display.
void LCD_clear(void);

// Function to write a block (contiguous series of lines) of data from the frame buffer to the LCD.
// Data block starts at *frmbffr, is written to LCD, starting at startLine, for nLines.
bool write_LCD_block(uint8_t startLine, uint8_t nLines, bool insideGlcd);


#endif	// LCD_H__
