/* Copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 * ************************************************************************
 *	March, 2014, version (used with SDK 4.4.2) has now been replaced with
 *	this version, compatible with SDK 6.1.
 *	By Mike Wirth, November, 2014.
 *	1. 	The new Nordic SPI driver issues events associated with SPI transfers.
 *			These should be used to avoid the blocking write below.
 *	2.	Chip select in the Nordic SPI hardware (code?) is active low.
 *			The Sharp LCD Select line is active high!  Therefore, chip select
 *			has to be handled in code in this file in LCD_spi_master_tx 
 *			AROUND the call to Nordic's spi_master_send_recv.
 * ************************************************************************
 *	Modified to drive a Sharp LS013B7DH03.
 *	By Mike Wirth, March, 2014.
 *	1. Code sections deleted for other than Mode 0 (clock low on idle, 
 *	sample data on rising edge, change on falling edge).
 *	2. LCD chip select line, SPI_PSELSS0 = LCD_SCS, is active high, not low.
 *	3. Receive pin is configured for "no connection". No data received from LCD.
 *	4. Clock is 1 MHz, pin assignments per PCB_pins.h (in LCD_spi_master.h)
 *	5. Specialized to module SPI0.
 *	6. Chip select line, LCD_SCS, is controlled external to LCD_spi_master_tx.
 * ************************************************************************
 */

#ifdef AMBIT
//#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "common.h"
#include "PCB_pins.h"
#include "LCD_spi_master_config.h" 
#include "LCD_spi_master.h"        // This file include must come after LCD_spi_master_config.h
#include "LCD.h"
#include "spi_master.h"
#include "app_util_platform.h"

//static inline __ASM void delay_us(uint32_t number_of_us)              // NOT CURENTLY USED
//{
//lp
//        SUBS    R0, R0, #1
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        NOP
//        BNE    lp
//        BX     LR
//}

static void setLcdChipSelect(void){
	nrf_gpio_pin_set(LCD_SCS);					// Turn on LCD chip select
	// Explicit delay not necessary.  Plenty of delay in code already (verified by logic analyzer)
	// delay_us(8);												// Per Sharp spec, > 6us
}

static void clearLcdChipSelect(void){
	// Explicit delay not necessary.  Plenty of delay in code already (verified by logic analyzer)
	//delay_us(3);												// Per Sharp spec, > 2us.
	nrf_gpio_pin_clear(LCD_SCS);				// Turn off LCD chip select
	//delay_us(8);												// Per Sharp spec, > 6 us
}

uint32_t LCD_spi_master_init(void)
{
	uint32_t errCode;
	// Setup configuration structure
	spi_master_config_t spi_config = SPI_MASTER_INIT_DEFAULT;	// Start with defaults. Then modify for Sharp LCD:
	spi_config.SPI_Freq 		= SPI_FREQUENCY_FREQUENCY_K500;		// Reduced to 0.5Mbps because of 0.525ms signal hold requirement of LCD
	spi_config.SPI_Pin_SCK 	= SPI_PSELSCK0;                   // Clock line.
	spi_config.SPI_Pin_MOSI = SPI_PSELMOSI0;									// MOSI line. MISO is left at default = disconnected.
	//spi_config.SPI_Pin_SS 	= SPI_PSELSS0;                  // Chip Select line has to be handled in LCD_spi_master_tx here
	// rather than in Nordic SPI SW/HW.  Leave DISCONNECTED for that.

	// Initialize and open SPI0 for the Sharp LCD.
	errCode = spi_master_open(SPI_MASTER_0, &spi_config);

	// Now setup LCD chip select line for handling by LCD_spi_master_tx.
	nrf_gpio_cfg_output(LCD_SCS);     // Note that LCD_SCS == SPI_PSELSS0
	clearLcdChipSelect();							// Inactive low.

	return errCode;
}

//	NOTE: THIS IS A BLOCKING WRITE TO THE LCD!  IT SHOULD BE IMPROVED BY REGISTERING FOR SPI EVENTS, STARTING
//	THE OPERATION AND RETURNING IMMEDIATELY. THEN HANDLE THE EVENT WHEN THE TRANSFER FINISHES LATER.  *********************
bool LCD_spi_master_tx(uint16_t transfer_size, uint8_t *tx_data)
{
	setLcdChipSelect();																									// Explicitly raise/make-active LCD-chip-select and delay a bit.
	spi_master_send_recv(SPI_MASTER_0,tx_data,transfer_size,NULL,0);
	while (spi_master_get_state(SPI_MASTER_0) == SPI_MASTER_STATE_BUSY) // Will this block if LCD fails? *******
	{}
	clearLcdChipSelect();																								// Delay a bit and drop LCD-chip-select
	return true;		
}

#endif	// AMBIT
