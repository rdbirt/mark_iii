/* Copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 * ************************************************************************
 *	Modified to drive a Sharp LS013B7DH03.
 *	By Mike Wirth, March, 2014.
 *	1. Code sections deleted for other than Mode 0 (clock low on idle, 
 *	sample data on rising edge, change on falling edge).
 *	2. LCD chip select line, SPI_PSELSS0 = LCD_SCS, is active high, not low.
 *	3. Receive pin is configured for "no connection". No data received from LCD.
 *	4. Clock is 1 MHz, pin assignments per PCB_pins.h (in LCD_spi_master.h)
 *	5. Specialized to module SPI0.
 *	6. Current version of LCD_spi_master_tx raises LCD_SCS at the beginning
 *	of each tranfer, and drops it at the end.
 * ************************************************************************
 */

#ifndef LCD_SPI_MASTER_H
#define LCD_SPI_MASTER_H

#include <stdbool.h>
#include <stdint.h>

///** @file
//* @brief Software controlled SPI Master driver 
//*
//*
//* @defgroup lib_driver_spi_master Software controlled SPI Master driver
//* @{
//* @ingroup nrf_drivers
//* @brief Software controlled SPI Master driver.
//*
//* Supported features:
//* - Operate two SPI masters independently or in parallel.
//* - Transmit and Receive given size of data through SPI.
//* - configure each SPI module separately through @ref LCD_spi_master_init.
//*/

///**
// *  SPI master operating frequency
// */
//typedef enum
//{
//    Freq_125Kbps = 0,        /*!< drive SClk with frequency 125Kbps */
//    Freq_250Kbps,            /*!< drive SClk with frequency 250Kbps */
//    Freq_500Kbps,            /*!< drive SClk with frequency 500Kbps */
//    Freq_1Mbps,              /*!< drive SClk with frequency 1Mbps */
//    Freq_2Mbps,              /*!< drive SClk with frequency 2Mbps */
//    Freq_4Mbps,              /*!< drive SClk with frequency 4Mbps */
//    Freq_8Mbps               /*!< drive SClk with frequency 8Mbps */
//} SPIFrequency_t;

/////**
//// *  SPI master module number
//// */
////typedef enum
////{
////    SPI0 = 0,               /*!< SPI module 0 */
////    SPI1                    /*!< SPI module 1 */
////} SPIModuleNumber;

/////**
//// *  SPI mode
//// */
////typedef enum
////{
////    //------------------------Clock polarity 0, Clock starts with level 0-------------------------------------------
////    SPI_MODE0 = 0,          /*!< Sample data at rising edge of clock and shift serial data at falling edge */
////    SPI_MODE1,              /*!< sample data at falling edge of clock and shift serial data at rising edge */
////    //------------------------Clock polarity 1, Clock starts with level 1-------------------------------------------
////    SPI_MODE2,              /*!< sample data at falling edge of clock and shift serial data at rising edge */
////    SPI_MODE3               /*!< Sample data at rising edge of clock and shift serial data at falling edge */
////} SPIMode;


/**
 * @brief Function for initializing and opening SPI0 as a SPI master device, configured to communicate
 * 				with a Sharp LS013B7DH03 LCD as a slave device.
 *
 */
uint32_t LCD_spi_master_init(void);

/**
 * @brief Function for transmitting data over SPI bus to a Sharp LCD
 *
 *
 * @note Make sure at least transfer_size number of bytes is allocated in tx_data.
 *				This version is specialized for SPI0 and transmit only.  Received "data"
 *				(dummy values) are discarded.
 * @note Input data is assumed to be a line of data per Figures 4 through 9 of the
 *				Sharp Application Note "Sharp Memory LCD Technology". by Ken Green, Sept., 2013.
 * @note At entry to this routine, it is assumed that the Chip Select line for the
 *				LCD has already been activated (and will remain activated over multiple lines/
 *				calls to this routine).
 *
 * @param transfer_size  number of bytes to transmit over SPI0 master
 * @param tx_data pointer to the data that needs to be transmitted
 * @return
 * @retval true if transmit/reveive of transfer_size were completed.
 * @retval false if transmit/reveive of transfer_size were not completed.
 */
bool LCD_spi_master_tx(uint16_t transfer_size, uint8_t *tx_data);

/**
 *@}
 **/

#endif /* LCD_SPI_MASTER_H */
