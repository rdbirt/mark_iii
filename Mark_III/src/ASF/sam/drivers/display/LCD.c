/* Copyright (c) 2014 Ambit Network Systems. All Rights Reserved.
 *
 *	The information contained herein is property of Ambit Systems.
 *	Derived from Sharp app note, by Ken Green.
 *
 *  This heading must NOT be removed from the file.
 *	By Mike Wirth, March, 2014.  Loosely based on the LCD
 *	App Note from Sharp, by Ken Green.
 *
 */

/**
 * Driver for a Sharp LCD, LS013B7DH03.
 *	Note: Uses TIMER2 to generate 60 Hz Vcom signal.
 *		
 */

#ifdef	 AMBIT

#include "common_headers.h"
#include <stdint.h>
#include "LCD.h"
#include "LCD_spi_master.h"
#include "nrf_gpio.h"
#include "PCB_pins.h"
#include "app_error.h"
#include "nrf_delay.h"



// Working variables -- track state of frame buffer transfer to LCD.
uint8_t vcom = 0;		// Current state of vcom (pulsed at 60Hz)
static uint8_t *Frmbufptr;	// Pointer to current byte in frame buffer
static uint8_t LcdLineBufr[MLCD_BYTES_PER_LINE + 4];	// Local buffer to assemble write record
static uint8_t LcdLineAddress;	// Current line number being transmitted (limited to 8 bits!)
// NOTE: Counts from 1 to MLCD_YRES; NOT from 0.
//uint8_t  LCDframeBuffer[2];
uint8_t reverseB(uint8_t b){
	uint8_t i, c=0;
	for (i=0;i<=6;i++){
		c |= b & 0x01;
		c = c<<1;
		b = b>>1;	
	}
	c |= b & 0x01;
	return c;
}

// Function to initialize the Sharp LCD, including the GPIO lines and SPI
// peripheral on the Nordic ARM chip.
// NOTE: Sequence of operations structured to follow Fig. 4, "Power Supply Sequencing" in document
// "Sharp LS013B7DH03 Application Information", p.7
void LCD_init(void)
{
	// Initialize SPI0 (SCLK, SI lines) and SCS GPIO line.
	LCD_spi_master_init();

	// T0 and T1 per Fig. 4.
	// Set up GPIO control lines (EXTCOMIN, DISP, EXTMODE)
	nrf_gpio_cfg_output(LCD_DISP);			// Used to turn display on/off
	nrf_gpio_cfg_output(LCD_EXTMODE);		// Controls source of VCOM
	nrf_gpio_cfg_output(LCD_EXTCOMIN);	// "VCOM" LCD commuting signal

	nrf_gpio_pin_clear(LCD_DISP);				// Start with LCD_DISP low,
	nrf_gpio_pin_clear(LCD_EXTCOMIN);		// 	  LCD_EXTCOMIN low, and
	nrf_gpio_pin_set(LCD_EXTMODE);			// 		selected as VCOM input.

	nrf_delay_ms(1);										// Wait for LCD latches to settle

	// Fig. 4, T2
	LCD_clear();												// With LCD_DISP off, clear the display
	nrf_delay_ms(1);										// For safety, wait a bit.

	// Fig. 4, T3
	nrf_gpio_pin_set(LCD_DISP);					// Raise LCD_DISP to turn on display of pixels, will stay on...
	nrf_delay_ms(1);										// For safety, wait a bit.

	return;															// LCD now in running mode
}


// Function to write a line of data from the frame buffer to the LCD.
// firstP is true if this is the first (possibly only) line to be written
// lastP is true if this is the last (possibly only) line to be written
// Returns true if successful, false otherwise.
bool write_LCD_line(uint8_t *dataPtr, bool firstP, bool lastP, bool insideGlcd)
{
	int32_t i;
	uint8_t *linebufPtr = &LcdLineBufr[0];	// Assemble data in external line buffer, LcdLineBufr.
	// Initialize working variables
	Frmbufptr = dataPtr;	// Copy arg value to global working variable.  // *****TBD******* Change to local after debugging

	//	If this is "firstline", we need to preface TX bytes with a Write command
	if (firstP) {
		*linebufPtr++ = MLCD_WR | vcom;   // vcom setting ignored when external clock is used
	}
	// Add LCD line number/Y-address
	*linebufPtr++ =  LcdLineAddress;			// Note -- LcdLineAddress gets updated outside ths function.

	// Transfer data from frame buffer to line buffer
	for (i=0;i < MLCD_BYTES_PER_LINE;i++) *linebufPtr++ = *Frmbufptr++;
	// Complete line buffer with one zero-byte trailer.
	*linebufPtr++ = 0;
	// If this is the last line, add a second zero-byte trailer
	if (lastP) *linebufPtr++ = 0;
	// Transer the complete record (bytes LcdLineBufr[0] to LcdLineBufr[linebufPtr-&LcdLineBufr[0]]
	return LCD_spi_master_tx(linebufPtr-&LcdLineBufr[0], &LcdLineBufr[0]);
}


// Function to clear the Sharp LCD display.
void LCD_clear(void)
{
	bool result;
	LcdLineBufr[0] = MLCD_CM;						// "Clear Memory" command
	LcdLineBufr[1] = 0x00;							// Trailer byte
	result = LCD_spi_master_tx(2,LcdLineBufr);	// Transfer 2-byte record (presume success)
	APP_ERROR_CHECK_BOOL(result);
	LcdLineAddress = 1;											// Set the LCD line number (Y-address) to 1 (ranges from 1 to MLCD_YRES).
}



// Function to write a block (series of lines) of data from the frame buffer to the LCD.
// Data block is written to LCD, starting at startLine, and continues for nLines.
// Note: 	Inside GLCD code, line numbers count from 0
//				Inside LCD code, line addresses count from 1
// Note: 
bool write_LCD_block(uint8_t startLine, uint8_t nLines, bool insideGlcd)
{
	uint8_t i; 														// Current (relative) line number in sequence, 1...nLines
	uint8_t *dataPtr;											// Pointer to first byte of startLine in frame buffer (if insideGlcd)
	bool successF = true;
	//	bool firstP, lastP;                   // Flags for first and last line of block.
	uint8_t n = nLines;                   // Internal line-remaining counter

	// Setup dataPtr and LcdLineAddress, accounting for difference in line numbering between GLCD and LCD modules
	LcdLineAddress = startLine;           // Note that LcdLineAddress is a global (for debugging purposes)
	if (insideGlcd) LcdLineAddress++;			// GLCD library numbers lines from 0....MLCD_YRES-1, Sharp LCD from 1...MLCD_YRES
	dataPtr = &LCDframeBuffer[(LcdLineAddress-1)*MLCD_BYTES_PER_LINE];		// Pointer to first byte of startLine in frame buffer


	if (LcdLineAddress < 1) {												// Check and correct starting line number
		uint8_t delta = 1 - LcdLineAddress;
		LcdLineAddress = 1;
		n -= delta;
	}
	if (LcdLineAddress + n - 1 > MLCD_YRES)	{				// Check end of range...
		uint8_t delta = MLCD_YRES-LcdLineAddress-n+1;
		n += delta;
	}

	// NOTE: THERE IS CURRENTLY SOME TIMING BUG IN LOWER LEVEL CODE THAT DRIVES THE SHARP LCD,
	// OR IN THE HARDWARE ITSELF AND MULTIPLE LINE WRITES DO NOT WORK.  THEREFORE, FOR NOW
	// WRITE THE BLOCK OF LINES AS A SERIES OF SINGLE LINE WRITES.
	for (i=1; i<=n; i++) {
		successF = write_LCD_line(dataPtr, true, true, insideGlcd); // Write a single line
		if (!successF) break;																				// Quit early on error
		dataPtr += MLCD_BYTES_PER_LINE;
		LcdLineAddress++;
	}
	return successF;

	//	// Write first and intermediate lines
	//	firstP = true;
	//	for (i=1; i <= n; i++)            // Count lines 1..n.
	//	{
	//		lastP = (i == nLines);
	//		successF = write_LCD_line(dataPtr, firstP, lastP, insideGlcd);	// Write a line.
	//		if (!successF) break;								// Quit early on error.
	//		firstP = false;                     // Turn off firstP
	//		dataPtr += MLCD_BYTES_PER_LINE;			// Move data pointer and line address to next line.
	//		LcdLineAddress++;
	//	}
	//	return successF;                  		// True if completed without error, false otherwise.
}


void forceAllocateFB(void)
{
	LCDframeBuffer[0] = 1;
}
/*	**************************************************************************************************
 *		LCD TEST PATTERNS
 *	**************************************************************************************************/
void paintCheckerboard(void) {
	uint16_t i, j;
	uint8_t *ptr = LCDframeBuffer;
	uint8_t c, black = 0xFF;
	c = black;
	for (j=0;j<8;j++) {
		for (i=0; i<128; i++) {
			*ptr++ = c;
			*ptr++ = c;
			c = ~c;
		}
		c = ~c;
	}
}

void paintDiagonalLine(void){

	uint32_t *wordPtr = (uint32_t *)&LCDframeBuffer[0];
	uint32_t w;
	uint8_t i,j,c1,c2;
	*wordPtr++ = 0x00000001UL;
	*wordPtr++ = 0x00000000UL;
	*wordPtr++ = 0x55555555UL;
	*wordPtr++ = 0x00000000UL;
	c1 = 0;
	for (i=2;i<=128;i++) {
		for (j=0; j<4; j++) {
			w = *(wordPtr-4);
			c2 = (w & 0x80000000UL) ? 1 : 0;	// Next carry bit
			*wordPtr++ = (w << 1) | c1;
			c1 = c2;
		}
	}
}

void dataIsLineNumber(void){
	uint8_t i,j;
	uint8_t *ptr = LCDframeBuffer;
	for (i=1;i<=128;i++){
		for(j=1;j<=16;j++)
			*ptr++ = i;
	}
}



#endif	// AMBIT
